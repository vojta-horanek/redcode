package eu.vojtechh.redcode.model.discogs

data class DGAlbum (
    val artists: List<DGArtist>,
    val title: String,
    val year: String,
)