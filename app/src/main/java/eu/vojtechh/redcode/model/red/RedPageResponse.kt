package eu.vojtechh.redcode.model.red

data class RedPageResponse<T>(
    val status: String,
    val response: RedPage<T>,
)