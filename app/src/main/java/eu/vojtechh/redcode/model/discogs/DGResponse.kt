package eu.vojtechh.redcode.model.discogs

data class DGResponse(
    val results: List<DGResult>
)