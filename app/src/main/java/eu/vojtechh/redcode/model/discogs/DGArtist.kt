package eu.vojtechh.redcode.model.discogs

data class DGArtist(
    val name: String
)