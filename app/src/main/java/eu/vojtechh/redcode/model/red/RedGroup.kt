package eu.vojtechh.redcode.model.red

data class RedGroup(
    val group: RedGroupDetails,
    val torrents: List<RedTorrent>
)
