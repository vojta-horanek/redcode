package eu.vojtechh.redcode.net.discogs

import eu.vojtechh.redcode.model.discogs.DGAlbum
import eu.vojtechh.redcode.model.discogs.DGResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface DGApiService {

    @GET("/database/search")
    suspend fun searchBarcode(
        @Header("Authorization") auth: String,
        @Query("barcode") barcode: String
    ): Response<DGResponse>

    @GET("/releases/{id}")
    suspend fun getRelease(
        @Header("Authorization") auth: String,
        @Path("id") releaseId: String
    ): Response<DGAlbum>

    @GET("/masters/{id}")
    suspend fun getMaster(
        @Header("Authorization") auth: String,
        @Path("id") masterId: String
    ): Response<DGAlbum>

}