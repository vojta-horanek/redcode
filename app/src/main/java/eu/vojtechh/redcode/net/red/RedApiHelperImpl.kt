package eu.vojtechh.redcode.net.red

import eu.vojtechh.redcode.model.red.*
import retrofit2.Response
import javax.inject.Inject

class RedApiHelperImpl @Inject constructor(
    private val redApiService: RedApiService
) : RedApiHelper {
    override suspend fun browse(auth: String, title: String?, artist: String?, year: String?):
            Response<RedPageResponse<RedRelease>> {
        if (artist == null && year == null) {
            return redApiService.search(auth, title)
        }
        return redApiService.browse(auth, title, artist, year)
    }

    override suspend fun request(auth: String, title: String?, artist: String?):
            Response<RedPageResponse<RedReleaseRequest>> {
        return if (artist == null) {
            redApiService.request(auth, "$title")
        } else {
            redApiService.request(auth, "$artist $title")
        }
    }

    override suspend fun groupDetails(
        auth: String,
        groupId: String
    ): Response<RedResponse<RedGroup>> = redApiService.groupDetails(auth, groupId)

}