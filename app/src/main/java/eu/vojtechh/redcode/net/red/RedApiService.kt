package eu.vojtechh.redcode.net.red

import eu.vojtechh.redcode.model.red.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface RedApiService {

    @GET("/ajax.php?action=browse")
    suspend fun browse(
        @Header("Authorization") auth: String,
        @Query("groupname") title: String?,
        @Query("artistname") artist: String?,
        @Query("year") year: String?,
    ): Response<RedPageResponse<RedRelease>>

    @GET("/ajax.php?action=requests")
    suspend fun request(
        @Header("Authorization") auth: String,
        @Query("search") searchQuery: String
    ): Response<RedPageResponse<RedReleaseRequest>>

    @GET("/ajax.php?action=torrentgroup")
    suspend fun groupDetails(
        @Header("Authorization") auth: String,
        @Query("id") groupId: String
    ): Response<RedResponse<RedGroup>>

    @GET("ajax.php?action=browse")
    suspend fun search(
        @Header("Authorization") auth: String,
        @Query("searchstr") query: String?
    ): Response<RedPageResponse<RedRelease>>
}