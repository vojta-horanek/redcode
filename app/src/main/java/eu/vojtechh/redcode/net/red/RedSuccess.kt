package eu.vojtechh.redcode.net.red

data class RedSuccess<T>(
    val type: RedType,
    val data: T
)