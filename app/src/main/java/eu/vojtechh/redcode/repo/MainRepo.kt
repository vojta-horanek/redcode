package eu.vojtechh.redcode.repo

import eu.vojtechh.redcode.model.Album
import eu.vojtechh.redcode.model.red.RedRelease
import eu.vojtechh.redcode.net.red.RedSuccess
import eu.vojtechh.redcode.util.Status
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MainRepo @Inject constructor(
    private val dgRepo: DGRepo,
    private val redRepo: RedRepo
) {
    fun lookup(barcode: String) = flow {
        // Search barcode on discogs
        dgRepo.searchBarcode(barcode).collect {
            // Emit status messages
            if (it.status != Status.SUCCESS) emit(it)
            else {
                // If we have something else then an album, emit it
                if (it.data !is Album) {
                    emit(it)
                } else {
                    // Search red for the album we are looking for
                    redRepo.searchRed(it.data).collect { redResource ->
                        // Emit status messages
                        if (redResource.status != Status.SUCCESS) emit(redResource)
                        else {
                            // If we found a release, fetch group details
                            if (redResource.data is RedSuccess<*> && redResource.data.data is RedRelease) {
                                redRepo.getGroupDetails(redResource.data.data.groupId)
                                    .collect { groupDetails ->
                                        emit(groupDetails)
                                    }
                            } else emit(redResource) // otherwise emit a request
                        }
                    }
                }
            }
        }
    }
}