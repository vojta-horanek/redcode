package eu.vojtechh.redcode.repo

import eu.vojtechh.redcode.model.Album
import eu.vojtechh.redcode.net.red.RedApiHelper
import eu.vojtechh.redcode.net.red.RedSuccess
import eu.vojtechh.redcode.net.red.RedType
import eu.vojtechh.redcode.util.Resource
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RedRepo @Inject constructor(private val redApiHelper: RedApiHelper) {
    fun searchRed(album: Album) = flow {
        emit(Resource.loading("Loading info from RED"))

        val auth = "ENTER TOKEN HERE"

        val title = album.title
        val artist = album.artistName
        val year = album.year

        val paramList = listOf(
            listOf(title, artist, year),
            listOf(title, artist, null),
            listOf(title, null, null)
        )

        emit(Resource.loading("Searching for groups"))
        for (params in paramList) {
            val search = redApiHelper.browse(auth, params[0], params[1], params[2])
            if (!search.isSuccessful) {
                emit(Resource.error(search.errorBody().toString(), null))
                return@flow
            } else {
                val responses = search.body()?.response?.results
                if (!responses.isNullOrEmpty()) {
                    emit(Resource.success(RedSuccess(RedType.RELEASE, responses.first())))
                    return@flow
                }
            }
        }

        val paramList2 = listOf(
            listOf(title, artist),
            listOf(title, null)
        )
        emit(Resource.loading("Searching for requests"))

        for (params in paramList) {
            val requestSearch = redApiHelper.request(auth, params[0], params[1])
            if (!requestSearch.isSuccessful) {
                emit(Resource.error(requestSearch.errorBody().toString(), null))
                return@flow
            } else {
                val responses = requestSearch.body()?.response?.results
                if (!responses.isNullOrEmpty()) {
                    emit(Resource.success(RedSuccess(RedType.REQUEST, responses.first())))
                    return@flow
                }
            }
        }

        emit(Resource.error("No records have been found on RED", null))
        return@flow
    }

    fun getGroupDetails(groupId: String) = flow {
        val auth = "6c05b742.4e822448f69f6d43f145004fc843d1a2"
        emit(Resource.loading("Fetching group details"))
        kotlinx.coroutines.delay(200)
        val groupRequest = redApiHelper.groupDetails(auth, groupId)
        if(!groupRequest.isSuccessful) {
            emit(Resource.error(groupRequest.errorBody().toString(), null))
            return@flow
        }
        emit(Resource.success(groupRequest.body()?.response))
        return@flow
    }
}
